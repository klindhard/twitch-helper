﻿using System;
using Crawlers;
using HelixApiClients;
using HelixCrawlers;
using StreamsClient;
using StreamsCore.Common;

namespace HelixGamesCrawlerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("AllGamesCrawler started");



            var clientCredentialsProvider = new ClientCredentialsProvider();

            var accessTokenProvider = new AccessTokenProvider(clientCredentialsProvider);



            var nowProvider = new NowProvider();
            var restClient = new Client(accessTokenProvider, nowProvider);

            var gamesClient = new GamesClient(restClient);

            var gamesCrawler = new AllGamesCrawler(
                gamesClient
            );


            gamesCrawler.Crawl().Wait();



        }
    }
}
