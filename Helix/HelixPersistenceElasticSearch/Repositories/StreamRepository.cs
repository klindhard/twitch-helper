﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelixRealm.Model;
using PersistenceElasticSearch.Interfaces;
using StreamsCore.Interfaces;

namespace PersistenceElasticSearch.Repositories
{
    public class StreamRepository : ICommandRepository<Stream>
    {
        private readonly IElasticClientCreator _elasticClientCreator;


        public StreamRepository(IElasticClientCreator elasticClientCreator)
        {
            _elasticClientCreator = elasticClientCreator;
        }


        public async Task AddAsync(IEnumerable<Stream> streams)
        {
            if (streams?.Any() != true) { return; }
            var elasticClient = await _elasticClientCreator.CreateAsync("streams");

            var tasks = new List<Task>();
            foreach (var stream in streams)
            {
                tasks.Add(elasticClient.IndexDocumentAsync(stream));

                Console.WriteLine($"{stream.CrawledAt:s} Id: {stream.Id} ViewerCount: {stream.ViewerCount.ToString().PadLeft(7, ' ')}");
            }


            Task.WaitAll(tasks.ToArray());
        }
    }
}
