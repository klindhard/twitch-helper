﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HelixRealm.Interfaces;
using RestSharp;
using StreamsCore.Interfaces;

namespace StreamsClient
{
    public class Client : IClient
    {
        private readonly INowProvider _nowProvider;
        private readonly RestClient _restClient;
        private readonly Dictionary<string, DateTime> _pauses = new Dictionary<string, DateTime>();

        public Client(IAccessTokenProvider accessTokenProvider, INowProvider nowProvider)
        {
            _nowProvider = nowProvider;

            var restClient = new RestClient("https://api.twitch.tv/");

            var getTokenTask = accessTokenProvider.GetTokenAsync();

            getTokenTask.Wait();

            restClient.AddDefaultHeader("Authorization", $"Bearer {getTokenTask.Result.AccessToken}");


            _restClient = restClient;
        }


        public async Task<IRestResponse> ExecuteTaskAsync(IRestRequest request, CancellationToken token)
        {
            var result = await _restClient.ExecuteTaskAsync(request, token);


            return result;
        }


        public async Task<IRestResponse> ExecuteTaskAsync(IRestRequest request)
        {
            await Delay(request.Resource);
            var result = await _restClient.ExecuteTaskAsync(request);
            SetPause(request.Resource, result.Headers);
            return result;
        }


        public async Task Delay(string resource)
        {
            DateTime needToPauseUntil;
            var thereIsAPause = _pauses.TryGetValue(
                resource,
                out needToPauseUntil
            );

            if (thereIsAPause == false) { return; }

            var delayTimeSpan = needToPauseUntil.Subtract(_nowProvider.GetNow());

            if (TimeSpan.Zero < delayTimeSpan)
            {
                await Task.Delay(delayTimeSpan);
            }

            _pauses.Remove(resource);
        }
        public void SetPause(string resource, IList<Parameter> headers)
        {
            var needToPauseUntil = NeedToPauseUntil(headers);

            if (needToPauseUntil == null) { return; }

            _pauses.Add(resource, needToPauseUntil.Value);
        }

        public DateTime? NeedToPauseUntil(IList<Parameter> headers)
        {
            var rateLimitRemainingParameter = headers
                .SingleOrDefault(p => p.Name == "Ratelimit-Remaining");

            if (rateLimitRemainingParameter == null) { return null; }
            if ((string)rateLimitRemainingParameter.Value != "10") { return null; }


            var rateLimitResetParameter = headers
                .Single(p => p.Name == "Ratelimit-Reset");

            var unitReleaseTime = Convert.ToInt32(rateLimitResetParameter.Value);

            var releaseTime = FromUnixTime(unitReleaseTime);


            return releaseTime;
        }


        public static DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }
    }
}
