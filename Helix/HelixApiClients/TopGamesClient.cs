﻿using System.Threading.Tasks;
using HelixRealm.Interfaces;
using HelixRealm.Model;
using RestSharp;
using RestSharp.Deserializers;

namespace HelixApiClients
{
    public class TopGamesClient : ITopGamesClient 
    {
       
        private readonly IClient _client;

        public TopGamesClient(IClient client)
        {
            _client = client;
        }

        public async Task<Games> GetAsync(string cursor)
        {
            var deserializer = new JsonDeserializer();
            var request = new RestRequest("helix/games/top", Method.GET);
            request.AddParameter("first", "100");

            if (string.IsNullOrEmpty(cursor) == false)
            {
                var afterParameter = new Parameter
                {
                    Name = "after",
                    Type = ParameterType.QueryString,
                    Value = cursor
                };
                request.Parameters.Add(afterParameter);
            }

            var response = await _client.ExecuteTaskAsync(request);
            var streams = deserializer.Deserialize<Games>(response);


            return streams;
        }
    }
}
