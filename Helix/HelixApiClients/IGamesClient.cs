﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HelixRealm.Model;

namespace HelixApiClients
{
    public interface IGamesClient
    {
        Task<Games> GetAsync(GameQueryIdentifier identifier, IEnumerable<string> ids);
    }

}
