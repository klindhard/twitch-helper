﻿using System.Threading.Tasks;
using HelixRealm.Model;

namespace APIClient
{
    public interface IStreamsClient
    {
        Task<Streams> GetAsync(string cursor);
    }
}
