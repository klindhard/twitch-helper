﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HelixRealm.Interfaces;
using HelixRealm.Model;
using RestSharp;
using StreamsCore.Interfaces;

namespace StreamsClient
{
    public class AccessTokenProvider : IAccessTokenProvider
    {
        private readonly IClientCredentialsProvider _clientCredentialsProvider;

        private Token _token;
        public AccessTokenProvider(
            IClientCredentialsProvider clientCredentialsProvider
        )
        {
            _clientCredentialsProvider = clientCredentialsProvider;
        }

        public async Task<Token> GetTokenAsync()
        {
            if (DateTime.UtcNow < _token?.ExpiresAt)
            {
                return _token;
            }

            var tokenFromTwitch = await GetTokenFromTwitchAsync();

            _token = tokenFromTwitch;


            return tokenFromTwitch;
        }


        public async Task<Token> GetTokenFromTwitchAsync()
        {
            var clientCredentials = await _clientCredentialsProvider.GetClientCredentialsAsync();

            var request = new RestRequest("kraken/oauth2/token") { Method = Method.POST };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("client_id", clientCredentials.Id);
            request.AddParameter("client_secret", clientCredentials.Secret);
            request.AddParameter("grant_type", "client_credentials");

            var baseUrl = "https://api.twitch.tv";
            var restClient = new RestClient(baseUrl);


            var response = restClient.Execute<Dictionary<string,string>>(request);


            var token = new Token();
            string accessToken;
            response.Data.TryGetValue("access_token", out accessToken);
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new Exception($"Could not extract access_token from auth response");
            }

            token.AccessToken = accessToken;

            string expiresInString;
            response.Data.TryGetValue("expires_in", out expiresInString);
            if (string.IsNullOrEmpty(expiresInString))
            {
                throw new Exception($"Could not extract expires_in from auth response");
            }

            int expiresIn;
            var couldParse = Int32.TryParse(expiresInString, out expiresIn);
            if (couldParse == false)
            {
                throw new Exception($"Could not convert: \"{expiresInString}\" to int");
            }

            var expiresAt = DateTime.UtcNow
                .AddSeconds(expiresIn)
                .Subtract(TimeSpan.FromSeconds(10));

            token.ExpiresAt = expiresAt;


            return token;
        }
    }

}
