﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelixRealm.Interfaces;
using HelixRealm.Model;
using RestSharp;
using RestSharp.Deserializers;

namespace HelixApiClients
{
    public enum GameQueryIdentifier
    {
        name,
        id
    }


 
    public class GamesClient : IGamesClient
    {
        private readonly IClient _client;

        public GamesClient(IClient client)
        {
            _client = client;
        }


        public async Task<Games> GetAsync(GameQueryIdentifier identifier, IEnumerable<string> ids)
        {
            const int maxIdsCount = 100;
            if (maxIdsCount < ids.Count()) { throw new ArgumentException($"{nameof(ids)} may not be over {maxIdsCount}"); }

            var deserializer = new JsonDeserializer();
            var request = new RestRequest("helix/games", Method.GET);

            foreach (var id in ids)
            {
                request.AddParameter(
                    identifier.ToString(),
                    id
                );
            }

            var response = await _client.ExecuteTaskAsync(request);
            var games = deserializer.Deserialize<Games>(response);



            return games;
        }
    }
}
