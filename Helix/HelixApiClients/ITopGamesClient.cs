﻿using System.Threading.Tasks;
using HelixRealm.Model;

namespace HelixApiClients
{
    public interface ITopGamesClient
    {
        Task<Games> GetAsync(string cursor);

    }
}
