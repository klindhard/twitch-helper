﻿using System.Threading.Tasks;
using HelixRealm.Interfaces;
using HelixRealm.Model;
using RestSharp;
using RestSharp.Deserializers;
using StreamsCore.Interfaces;

namespace APIClient
{
    public class StreamsClient : IStreamsClient
    {
        private readonly IClient _client;

        public StreamsClient(IClient client)
        {
            _client = client;
        }


        public async Task<Streams> GetAsync(string cursor)
        {
            var deserializer = new JsonDeserializer();
            var request = new RestRequest("helix/streams", Method.GET);
            request.AddParameter("first", "100");

            if (string.IsNullOrEmpty(cursor) == false)
            {
                var afterParameter = new Parameter
                {
                    Name = "after",
                    Type = ParameterType.QueryString,
                    Value = cursor
                };
                request.Parameters.Add(afterParameter);
            }

            var response = await _client.ExecuteTaskAsync(request);
            var streams = deserializer.Deserialize<Streams>(response);

          
            return streams;
        }
    }
}
