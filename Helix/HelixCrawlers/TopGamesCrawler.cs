﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelixApiClients;
using HelixRealm.Model;
using StreamsCore.Common;
using StreamsCore.Interfaces;

namespace HelixCrawlers
{
    public class TopGamesCrawler
    {
        private readonly INowProvider _nowProvider;
        private readonly ITopGamesClient _topGamesClient;
        private readonly ICorrelationTokenIdCreator _correlationTokenIdCreator;
        public TopGamesCrawler(
            INowProvider nowProvider,
            ITopGamesClient topGamesClient,
            ICorrelationTokenIdCreator correlationTokenIdCreator
        )
        {
            _nowProvider = nowProvider;
            _topGamesClient = topGamesClient;
            _correlationTokenIdCreator = correlationTokenIdCreator;
        }

        public async Task Crawl()
        {
            var correlationTokenId = await _correlationTokenIdCreator.CreateAsync();
            var getTasks = new List<Task<Games>>();
            getTasks.Add(GetGamesAsync(getTasks, null));

            var persistTasks = new List<Task>();

            while (getTasks.Any())
            {
                var getTask = await Task.WhenAny(getTasks);
                getTasks.Remove(getTask);

                //var decorateTask = getTask
                //    .ContinueWith(t =>
                //            DecorateStreamsAsync(t.Result.Data, correlationTokenId),
                //        TaskContinuationOptions.OnlyOnRanToCompletion
                //    );

                //var persistTask = decorateTask.ContinueWith(t =>
                //    _streamCommandRepository.AddAsync(t.Result?.Result));
                //persistTasks.Add(persistTask);½
            }

            Task.WaitAll(getTasks.ToArray());

        //    Task.WaitAll(persistTasks.ToArray());
        }


        public async Task<Games> GetGamesAsync(
            List<Task<Games>> taskList,
            string cursor
        )
        {
            var games = await _topGamesClient.GetAsync(cursor);

            if (games.Pagination?.Cursor != null)
            {
                taskList.Add(GetGamesAsync(taskList, games.Pagination?.Cursor));
            }


            return games;
        }

    }
}
