﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIClient;
using HelixRealm.Model;
using StreamsCore.Common;
using StreamsCore.Interfaces;

namespace Crawlers
{
    public class StreamsCrawler
    {
        private readonly INowProvider _nowProvider;
        private readonly IStreamsClient _streamsClient;
        private readonly ICorrelationTokenIdCreator _correlationTokenIdCreator;
        private readonly ICommandRepository<Stream> _streamCommandRepository;
        public StreamsCrawler(
            INowProvider nowProvider,
            IStreamsClient streamsClient,
            ICorrelationTokenIdCreator correlationTokenIdCreator,
            ICommandRepository<Stream> streamCommandRepository
        )
        {
            _nowProvider = nowProvider;
            _streamsClient = streamsClient;
            _correlationTokenIdCreator = correlationTokenIdCreator;
            _streamCommandRepository = streamCommandRepository;
        }


        public async Task Crawl()
        {
            var correlationTokenId = await _correlationTokenIdCreator.CreateAsync();
            var getTasks = new List<Task<Streams>>();
            getTasks.Add(GetStreamsAsync(getTasks, null));

            var persistTasks = new List<Task>();

            while (getTasks.Any())
            {
                var getTask = await Task.WhenAny(getTasks);
                getTasks.Remove(getTask);

                var decorateTask = getTask
                    .ContinueWith(t => 
                        DecorateStreamsAsync(t.Result.Data, correlationTokenId),
                        TaskContinuationOptions.OnlyOnRanToCompletion
                    );

                var persistTask = decorateTask.ContinueWith(t => 
                    _streamCommandRepository.AddAsync(t.Result?.Result));
                persistTasks.Add(persistTask);
            }


            Task.WaitAll(persistTasks.ToArray());
        }



        public async Task<Streams> GetStreamsAsync(
            List<Task<Streams>> taskList,
            string cursor
        )
        {
            var streams = await _streamsClient.GetAsync(cursor);

            if (streams.Pagination?.Cursor != null)
            {
                taskList.Add(GetStreamsAsync(taskList, streams.Pagination?.Cursor));
            }


            return streams;
        }

        public Task<List<Stream>> DecorateStreamsAsync(
            IEnumerable<Stream> streams,
            string correlationTokenId
        )
        {
            if (streams == null) { return null; }

            var now = _nowProvider.GetNow();

            var resultStream = streams.Select(
                s =>
                {
                    s.CorrelationTokenId = correlationTokenId;
                    s.CrawledAt = now;

                    return s;
                }).ToList();


            return Task.FromResult(resultStream);
        }
    }
}
