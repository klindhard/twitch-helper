﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crawlers
{
    public interface ICrawler<T>
    {
        Task<IEnumerable<T>> Crawl();
    }
}
