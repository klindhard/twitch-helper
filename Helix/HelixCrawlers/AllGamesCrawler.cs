﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelixApiClients;
using HelixRealm.Model;

namespace HelixCrawlers
{
    public class AllGamesCrawler
    {
        private readonly IGamesClient _gamesClient;


        public AllGamesCrawler(IGamesClient gamesClient)
        {
            _gamesClient = gamesClient;
        }


        public async Task Crawl()
        {
            var getTasks = new List<Task<Games>>();
            getTasks.Add(
                GetGamesAsync(
                    getTasks,
                    34000));

            while (getTasks.Any())
            {
                var getTask = await Task.WhenAny(getTasks);
                getTasks.Remove(getTask);

                foreach (var game in getTask.Result.Data)
                {
                    Console.WriteLine($"id: {game.Id}, name: {game.Name}");
                }
            }

            Task.WaitAll(getTasks.ToArray());
        }


        public async Task<Games> GetGamesAsync(
        List<Task<Games>> taskList,
        int
            position
        )
        {
            var endposition = position + 100;
            var ids = new List<string>();
            while (position < endposition)
            {
                ids.Add(position.ToString());
                position++;
            }

            var games = await _gamesClient.GetAsync(GameQueryIdentifier.id, ids);

            taskList.Add(GetGamesAsync(taskList, position));
            // TODO Add logic to keep going to make sure we still dont have any games
            //if (games.Data?.Any() == true)
            //{
            //    taskList.Add(GetGamesAsync(taskList, position));
            //}


            return games;
        }
    }
}
