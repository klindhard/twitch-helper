﻿namespace HelixRealm.Model
{
    public class ClientCredentials
    {
        public string Id { get; set; }
        public string Secret { get; set; }
    }
}
