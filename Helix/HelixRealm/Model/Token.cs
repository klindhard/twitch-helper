﻿using System;

namespace HelixRealm.Model
{
    public class Token
    {
        public string AccessToken { get; set; }

        public DateTime ExpiresAt { get; set; }
    }
}
