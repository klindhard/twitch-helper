﻿using System;
using HelixRealm.Interfaces;
using StreamsCore.Interfaces;

namespace HelixRealm.Model
{
    public class Video : ICrawledResource, ICorrelationToken
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CreatedAt { get; set; }
        public string PublishedAt { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Viewable { get; set; }
        public string ViewCount { get; set; }
        public string Language { get; set; }
        public string Type { get; set; }
        public string Duration { get; set; }
        public DateTime CrawledAt { get; set; }
        public string CorrelationTokenId { get; set; }
    }
}
