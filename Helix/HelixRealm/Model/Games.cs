﻿using System.Collections.Generic;

namespace HelixRealm.Model
{
    public class Games
    {
        public List<Game> Data { get; set; }
        public Pagination Pagination { get; set; }
    }
}
