﻿using System.Collections.Generic;
using HelixRealm.Interfaces;

namespace HelixRealm.Model
{
    public class Streams : IStreams
    {
        public List<Stream> Data { get; set; }
        public Pagination Pagination { get; set; }
    }
}
