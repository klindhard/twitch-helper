﻿using System;
using System.Collections.Generic;
using HelixRealm.Interfaces;

namespace HelixRealm.Model
{
    public class Stream :IStream
    {
        public IEnumerable<string> CommunityIds { get; set; }
        public string GameId { get; set; }
        public string Id { get; set; }
        public string Language { get; set; }
        public string StartedAt { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string UserId { get; set; }
        public int ViewerCount { get; set; }
        public DateTime CrawledAt { get; set; }
        public string CorrelationTokenId { get; set; }
    }
}
