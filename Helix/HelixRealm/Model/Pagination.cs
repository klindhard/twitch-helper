﻿using HelixRealm.Interfaces;

namespace HelixRealm.Model
{
    public class Pagination :IPagination
    {
        public string Cursor { get; set; }
    }
}
