﻿using System.Collections.Generic;

namespace HelixRealm.Interfaces

{
    public class IStreams
    {
        IEnumerable<IStream> Data { get; set; }
        IPagination Pagination { get; set; }
    }
}
