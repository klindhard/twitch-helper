﻿namespace HelixRealm.Interfaces
{
    public interface IPagination
    {
        string Cursor { get; set; }
    }
}
