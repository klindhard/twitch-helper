﻿using System.Collections.Generic;
using StreamsCore.Interfaces;

namespace HelixRealm.Interfaces
{
    public interface IStream : ICrawledResource, ICorrelationToken
    {
        IEnumerable<string> CommunityIds { get; set; }
        string GameId { get; set; }
        string Id { get; set; }
        string Language { get; set; }
        string StartedAt { get; set; }
        string ThumbnailUrl { get; set; }
        string Title { get; set; }
        string Type{ get; set; }
        string UserId { get; set; }
        int ViewerCount { get; set; }
    }
}
