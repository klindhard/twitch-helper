﻿using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace HelixRealm.Interfaces
{
    public interface IClient
    {
        Task<IRestResponse> ExecuteTaskAsync(IRestRequest request, CancellationToken token);
        Task<IRestResponse> ExecuteTaskAsync(IRestRequest request);
    }
}
