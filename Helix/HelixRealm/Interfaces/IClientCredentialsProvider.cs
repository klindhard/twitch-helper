﻿using System.Threading.Tasks;
using HelixRealm.Model;

namespace HelixRealm.Interfaces
{
    public interface IClientCredentialsProvider
    {
        Task<ClientCredentials> GetClientCredentialsAsync();
    }
}
