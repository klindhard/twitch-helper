﻿using System.Threading.Tasks;
using HelixRealm.Model;

namespace HelixRealm.Interfaces
{
    public interface IAccessTokenProvider
    {
        Task<Token> GetTokenAsync();
    }
}
