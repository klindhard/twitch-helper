﻿using System;

namespace HelixRealm.Interfaces
{
    public interface ICrawledResource
    {
        DateTime CrawledAt { get; set; }
    }
}
