﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using APIClient;
using Castle.Windsor;
using Crawlers;
using HelixRealm.Model;
using Moq;
using StreamsCore.Common;
using StreamsCore.Interfaces;
using UnitTests.Core;
using Xunit;

namespace CrawlersUnitTests
{
    public class StreamsCrawlerFacts
    {
        [Fact]
        public async Task DecorateStreamsAsync_Can_Handle_Null_Streams()
        {
            var container = new WindsorContainer().Install(new GenericInstaller(typeof(StreamsCrawler)));
            var streamsCrawler = container.Resolve<StreamsCrawler>();

            container
                .Resolve<Mock<IStreamsClient>>()
                .Setup(x => x.GetAsync(It.IsAny<string>()))
                .ReturnsAsync(new Streams());


            // Act
            await streamsCrawler.Crawl();
        }
    }
}
