﻿using System;
using StreamsCore.Common;
using Xunit;

namespace StreamsCoreUnitTests.Common
{
    public class CorrelationTokenIdCreatorFacts
    {
        [Fact]
        public void UppercaseSegmentsAtRandom_Will_Return_The_Same_Characters()
        {
            // Arrange
            var vanillaGuid = Guid.NewGuid().ToString();
            var correlationTokenIdCreator = new CorrelationTokenIdCreator();


            // Act
            var result = correlationTokenIdCreator.UppercaseSegmentsAtRandom(vanillaGuid);


            // Assert 
            Assert.Equal(vanillaGuid, result.ToLower());
        }


        [Fact]
        public void UppercaseSegmentsAtRandom_Will_Uppercase_At_Least_One_Segment()
        {
            // Arrange
            var vanillaGuid = Guid.NewGuid().ToString();
            var correlationTokenIdCreator = new CorrelationTokenIdCreator();


            // Act
            var result = correlationTokenIdCreator.UppercaseSegmentsAtRandom(vanillaGuid);



            // Assert 
            var segments = result.Split('-');
            var hasAOnlyDigitSegment = false;
            var hasAUppercaseSegment = false;

            foreach (var segment in segments)
            {
                if (IsDigitsOnly(segment)) { hasAOnlyDigitSegment = true; continue; }
                if (IsAllUpperOrDigit(segment)) { hasAUppercaseSegment = true; }
            }

            Assert.True(hasAUppercaseSegment || hasAOnlyDigitSegment);
        }

        bool IsDigitsOnly(string str)
        {
            foreach (var c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }


        bool IsAllUpperOrDigit(string str)
        {
            foreach (var c in str)
            {
                if (Char.IsUpper(c) == false && (c < '0' || c > '9'))
                    return false;
            }

            return true;
        }
    }
}
