﻿using System;
using System.Diagnostics;
using PersistenceElasticSearch;
using PersistenceElasticSearch.Repositories;
using StreamsClient;
using StreamsCore.Common;

namespace ActiveStreamsCrawlerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("LiveStreamsCrawler started");
            var elasticClientCreator = new ElasticClientCreator(new ElasticSearchConnectionStringProvider());
            var healthCheck = new HealthCheck(elasticClientCreator);

            var isHealthy = healthCheck.IsHealthy().Result;
            Console.WriteLine($"Elastic connection is healthy? {isHealthy}");
            if (isHealthy == false) { return; }

            var clientCredentialsProvider = new ClientCredentialsProvider();

            var accessTokenProvider = new AccessTokenProvider(clientCredentialsProvider);

            var nowProvider = new NowProvider();
            var restClient = new Client(accessTokenProvider, nowProvider);

            var streamsClient = new APIClient.StreamsClient(restClient);
            var streamRepository = new StreamRepository(elasticClientCreator);
            var streamCrawler = new Crawlers.StreamsCrawler(
                nowProvider,
                streamsClient,
                new CorrelationTokenIdCreator(),
                streamRepository
            );

            while (true)
            {
                var stopwatch = Stopwatch.StartNew();
                streamCrawler.Crawl().Wait();
                Console.WriteLine($"Crawl is all done after {stopwatch.Elapsed:g}");
            }
        }
    }
}
