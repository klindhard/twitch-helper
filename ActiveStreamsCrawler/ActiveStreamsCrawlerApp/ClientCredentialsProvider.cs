﻿using System;
using System.Threading.Tasks;
using HelixRealm.Interfaces;
using HelixRealm.Model;
using StreamsCore.Interfaces;

namespace ActiveStreamsCrawlerApp
{
    public class ClientCredentialsProvider : IClientCredentialsProvider
    {
        private const string TwitchClientId = "TwitchClientId";
        private const string TwitchClientSecret = "TwitchClientSecret";


        public Task<ClientCredentials> GetClientCredentialsAsync()
        {
            var clientCredentials = new ClientCredentials
            {
                Id = Environment.GetEnvironmentVariable(TwitchClientId) ?? throw new Exception($"Could not retrieve environment variable: {TwitchClientId}"),
                Secret = Environment.GetEnvironmentVariable(TwitchClientSecret) ?? throw new Exception($"Could not retrieve environment variable: {TwitchClientSecret}")
            };



            return Task.FromResult(clientCredentials);
        }
    }
}
