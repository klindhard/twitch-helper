﻿using System;
using StreamsCore.Interfaces;

namespace StreamsClient
{
    public class NowProvider :INowProvider
    {
        public DateTime GetNow()
        {
            return DateTime.UtcNow;
        }
    }
}
