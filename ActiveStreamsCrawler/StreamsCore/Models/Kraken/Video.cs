﻿using System.Collections.Generic;

namespace StreamsCore.Models.Kraken
{
    public class Video
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string DescriptionHtml { get; set; }
        public string BroadcastId { get; set; }
        public string BroadcastType { get; set; }
        public string Status { get; set; }
        public string Language { get; set; }
        public string TagList { get; set; } //is thus a array?
        public string Views { get; set; }
        public string CreatedAt { get; set; }
        public string PublishedAt { get; set; }
        public string Url { get; set; }
        public string Id { get; set; }
        public string RecordedAt { get; set; }
        public string Game { get; set; }
        public string Length { get; set; }
        public string Preview { get; set; }
        public string AnimatedPreviewUrl { get; set; }
        public List<Thumbnail> Thumbnails { get; set; }
        public Dictionary<string, decimal> Fps { get; set; }
        public Dictionary<string, string> Resolutions { get; set; }
        public Channel Channel { get; set; }
        public Links Links { get; set; }
    }
}
