﻿namespace StreamsCore.Models.Kraken
{
    public class Links
    {
        public string Self { get; set; }
        public string Channel { get; set; }
    }
}
