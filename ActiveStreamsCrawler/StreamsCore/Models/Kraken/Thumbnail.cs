﻿namespace StreamsCore.Models.Kraken
{
    public class Thumbnail
    {
        public string Type { get; set; }
        public string Url { get; set; }
    }
}
