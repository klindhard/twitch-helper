﻿namespace StreamsCore.Models.Kraken
{
    public class Channel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}
