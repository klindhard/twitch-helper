﻿using System.Threading.Tasks;
using Nest;

namespace PersistenceElasticSearch
{
    public interface IElasticClientCreator
    {
        Task<IElasticClient> CreateAsync();
        Task<IElasticClient> CreateAsync(string defaultIndex);
    }
}
