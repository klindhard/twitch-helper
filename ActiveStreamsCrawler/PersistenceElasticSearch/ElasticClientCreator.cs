﻿using System.Threading.Tasks;
using Nest;

namespace PersistenceElasticSearch
{
    public class ElasticClientCreator : IElasticClientCreator
    {
        private readonly IElasticSearchConnectionStringProvider _elasticSearchConnectionStringProvider;


        public ElasticClientCreator(IElasticSearchConnectionStringProvider elasticSearchConnectionStringProvider)
        {
            _elasticSearchConnectionStringProvider = elasticSearchConnectionStringProvider;
        }


        public async Task<IElasticClient> CreateAsync()
        {
            var settings = new
                ConnectionSettings(await _elasticSearchConnectionStringProvider.GetConnectionStringAsync());

            var client = new ElasticClient(settings);


            return client;
        }


        public async Task<IElasticClient> CreateAsync(string defaultIndex)
        {
            var settings = new 
                ConnectionSettings(await _elasticSearchConnectionStringProvider.GetConnectionStringAsync())
                .DefaultIndex(defaultIndex)
                .DisableDirectStreaming();

            var client = new ElasticClient(settings);


            return client;
        }
    }
}
