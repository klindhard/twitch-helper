﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace PersistenceElasticSearch
{
    public class HealthCheck
    {
        private readonly IElasticClientCreator _elasticClientCreator;


        public HealthCheck(IElasticClientCreator elasticClientCreator)
        {
            _elasticClientCreator = elasticClientCreator;
        }


        public async Task<bool> IsHealthy()
        {
            var client = await _elasticClientCreator.CreateAsync();

            var health = await client.CatHealthAsync();

            if (health.IsValid == false) { return false; }


            var firstRecord = health.Records.First();

            if (firstRecord.Status == "green") { return true; }
            if (firstRecord.Status == "yellow" && firstRecord.NodeTotal == "1") { return true; }


            return false;
        }
    }
}
