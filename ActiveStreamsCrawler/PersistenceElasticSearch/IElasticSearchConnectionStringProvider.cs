﻿using System;
using System.Threading.Tasks;

namespace PersistenceElasticSearch
{
    public interface IElasticSearchConnectionStringProvider
    {
        Task<Uri> GetConnectionStringAsync();
    }
}
