﻿using System;
using System.Threading.Tasks;

namespace PersistenceElasticSearch
{
    public class ElasticSearchConnectionStringProvider : IElasticSearchConnectionStringProvider
    {
        public Task<Uri> GetConnectionStringAsync()
        {
            return Task.FromResult(new Uri("http://35.204.139.134:9200/"));
        }
    }
}
