﻿using System;
using System.Threading.Tasks;

namespace StreamsCore.Common
{
    public class CorrelationTokenIdCreator : ICorrelationTokenIdCreator
    {
        public Task<string> CreateAsync()
        {
            var vanillaGuid = Guid.NewGuid().ToString();

            var result = UppercaseSegmentsAtRandom(vanillaGuid);


            return Task.FromResult(result);
        }


        public string UppercaseSegmentsAtRandom(string sourceGuid)
        {
            var segments = new[]
            {
                sourceGuid.Substring(0, 8),
                sourceGuid.Substring(9, 4),
                sourceGuid.Substring(14, 4),
                sourceGuid.Substring(19, 4),
                sourceGuid.Substring(24, 12)
            };

            var random = new Random();
            var atLeastOneSegmentIsUpperCase = false;
            for (var i = 0; i < segments.Length; i++)
            {
                var shouldUpperCase = random.NextDouble() > 0.5;
                if (shouldUpperCase == false) { continue; }
                segments[i] = segments[i].ToUpper();
                atLeastOneSegmentIsUpperCase = true;
            }

            if (atLeastOneSegmentIsUpperCase == false) { segments[5] = segments[5].ToUpper(); }


            var result = string.Join('-', segments);


            return result;
        }
    }
}
