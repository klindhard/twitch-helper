﻿using System.Threading.Tasks;

namespace StreamsCore.Common
{
    public interface ICorrelationTokenIdCreator
    {
        Task<string> CreateAsync();
    }
}