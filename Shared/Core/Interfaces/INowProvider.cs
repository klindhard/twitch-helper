﻿using System;

namespace StreamsCore.Interfaces
{
    public interface INowProvider
    {
        DateTime GetNow();
    }
}
