﻿namespace StreamsCore.Interfaces
{
    public interface ICorrelationToken
    {
        string CorrelationTokenId { get; }
    }
}
