﻿using System;
using StreamsCore.Interfaces;

namespace StreamsCore.Common
{
    public class NowProvider :INowProvider
    {
        public DateTime GetNow()
        {
            return DateTime.UtcNow;
        }
    }
}
