﻿using System.Threading.Tasks;

namespace StreamsCore.Interfaces
{
    public interface IProvider<T>
    {
        Task<T> GetAsync();
    }
}
