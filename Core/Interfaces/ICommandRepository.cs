﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StreamsCore.Interfaces
{
    public interface ICommandRepository<T>
    {
        Task AddAsync(IEnumerable<T> itemsToAdd);
    }
}
