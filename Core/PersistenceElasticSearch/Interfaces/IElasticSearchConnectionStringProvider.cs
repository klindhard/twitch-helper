﻿using System;
using System.Threading.Tasks;

namespace PersistenceElasticSearch.Interfaces
{
    public interface IElasticSearchConnectionStringProvider
    {
        Task<Uri> GetConnectionStringAsync();
    }
}
