﻿using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Moq;

namespace UnitTests.Core
{
    public class AutoMoqResolver : ISubDependencyResolver
    {
        private readonly IKernel _kernel;

        public AutoMoqResolver(IKernel kernel)
        {
            _kernel = kernel;
        }

        public bool CanResolve(
            CreationContext context,
            ISubDependencyResolver contextHandlerResolver,
            ComponentModel model,
            DependencyModel dependency)
        {
            return dependency.TargetType.IsInterface;
        }

        public object Resolve(
            CreationContext context,
            ISubDependencyResolver contextHandlerResolver,
            ComponentModel model,
            DependencyModel dependency)
        {
            var mockType = typeof(Mock<>).MakeGenericType(dependency.TargetType);

            var mockObject = ((Mock) _kernel.Resolve(mockType)).Object;


            return mockObject;
        }
    }
}
