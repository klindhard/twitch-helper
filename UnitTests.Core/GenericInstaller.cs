﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Moq;
using System;

namespace UnitTests.Core
{
    public class GenericInstaller : IWindsorInstaller
    {
        private readonly Type _fromAssemblyContainingType;


        public GenericInstaller(Type fromAssemblyContainingType)
        {
            _fromAssemblyContainingType = fromAssemblyContainingType;
        }
        public void Install(
            IWindsorContainer container,
            IConfigurationStore store)
        {
            container.Kernel.Resolver.AddSubResolver(
                new AutoMoqResolver(
                    container.Kernel));
            container.Register(Component
                .For(typeof(Mock<>)));

            container.Register(Classes
                .FromAssemblyContaining(_fromAssemblyContainingType)
                .Pick()
                .WithServiceSelf()
                .LifestyleTransient());
        }


        public static IWindsorContainer CreateContainer(Type fromAssemblyContainingType)
        {
            var container = new WindsorContainer().Install(new GenericInstaller(fromAssemblyContainingType));


            return container;
        }
    }

}
